$(function() {
    defaultRedirectUri = "vvs.html";
    queryParams = new URLSearchParams(document.location.search.substring(1));
    redirectUri = queryParams.get('redirect_uri') || defaultRedirectUri;

    function getConfig(beforeCallbackFn, successCallbackFn, errorCallbackFn) {
        if(beforeCallbackFn) {
            beforeCallbackFn();
        }
        $.ajax({
            url: "assets/config.json",
            type: "GET",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",

            success: (configData) => {
                console.log({configData});
                let configJson = JSON.stringify(configData);
                localStorage.setItem("config", configJson);
                if(successCallbackFn) {
                    successCallbackFn();
                }
            },
            error: () => {
                console.log("error in getting configuration json");
                if(errorCallbackFn) {
                    errorCallbackFn();
                }
            }
        });
    }

    function getUrlParts(url) {
        let a = document.createElement('a');
        a.href = url;

        return {
            href: a.href,
            host: a.host,
            hostname: a.hostname,
            port: a.port,
            pathname: a.pathname,
            protocol: a.protocol,
            hash: a.hash,
            search: a.search
        };
    }

    function initAuthorizationProtocal() {
        let localConfigJson = localStorage.getItem('config');
        if(!localConfigJson) {
            console.log('config is not stored in localStorage');
            return null;
        }
        let localConfig = JSON.parse(localConfigJson);
        let platformUrl = localConfig['platform_url'];
        let clientDetails = localConfig['oauth_client'];

        let queryParams = {
          "client_id": clientDetails['client_id'],
          "response_type": "token",
          "scope": "vedavaapi.root",
          "redirect_uri": encodeURIComponent(getUrlParts('oauth_callback.html').href),
          "state": encodeURIComponent(redirectUri)
        };
        let qstr = Object.keys(queryParams).reduce((ps, cs) => `${ps}&${cs}=${queryParams[cs]}`, '').substring(1);
        let fullAuthorizationUri = `${platformUrl}/accounts/oauth/v1/authorize?${qstr}`;
        window.location.replace(fullAuthorizationUri);
    }

    $("#auth-button").click(() => {
        initAuthorizationProtocal();
    });


    let cfgbcbfn = () => {
        $("#status-cell").text('getting configuration......');
    };
    let cfgscbfn = () => {
        $("#status-cell").text('initializing auth procedure');
        $("#auth-block").css({"display": "block"});
    };
    let cfgecbfn = () => {
        $("#status-cell").text('configuration fetching failure!');
    };

    getConfig(cfgbcbfn, cfgscbfn, cfgecbfn);

})