queryParams = new URLSearchParams(document.location.hash.substring(1));

function persistAuthData() {
    authData = {}
    for(let k of queryParams.keys()) {
        authData[k] = queryParams.get(k);
    }
    authDataJson = JSON.stringify(authData);
    localStorage.setItem('authData', authDataJson);
}

function performRedirection() {
    let redirectUri  = queryParams.get('state');
    //let fullRedirectUri = `${redirectUri}?${document.location.hash.substring(1)}`;
    window.location.replace(redirectUri);
}

persistAuthData();
performRedirection();

