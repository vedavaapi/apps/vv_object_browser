
function ShellInterfaceException(message) {
    this.name = 'ShellBridgeException';
    this.message = message;
}

function ShellInterface(vvSiteUrl, applicationId) {
    this.vvSiteUrl = vvSiteUrl;
    this.applicationId = applicationId;
}

ShellInterface.fromQueryParams = function fromQueryParams() {
    let queryParams = new URLSearchParams(document.location.search.substring(1));

    let vvSiteUrl = queryParams.get('vv_site_url');
    let accessToken = queryParams.get('access_token') || '';
    let applicationId = queryParams.get('application_id') || '_';
    
    if(vvSiteUrl) {
        sessionStorage.setItem('vv_site_url', vvSiteUrl);
    }
    sessionStorage.setItem('application_id', applicationId);
    let shellInterface = ShellInterface.bootstrapFromSessionStorage();

    if(accessToken) {
        let authorizationDetails = {
            "access_token": accessToken
        }
        shellInterface.persistAuthorizationDetails(authorizationDetails);
    }
    return shellInterface;
}

ShellInterface.bootstrapFromSessionStorage = function bootstrapFromSessionStorage() {
    let vvSiteUrl = sessionStorage.getItem('vv_site_url');
    let applicationId = sessionStorage.getItem('application_id');
    if (!vvSiteUrl) {
        throw new ShellInterfaceException('vv_site_url is not provided in queryParams');
    }
    return new ShellInterface(vvSiteUrl, applicationId);
}


ShellInterface.prototype = {
    shellAuthorizationsKey: 'shell_authorizations',
    vvSiteUrlKey: 'vv_site_url',
    applicationIdKey: 'application_id',

    authorizationRequestMessageType : 'authorization_request',
    authorizationChangeMessageType : 'authorization_change',

    retrieveAuthorizationDetails() {
        let persistedString = sessionStorage.getItem(this.shellAuthorizationsKey) || '{}';
        let persistedObj = JSON.parse(persistedString);
        let siteAuthorizationDetails = persistedObj[this.vvSiteUrl];
        return siteAuthorizationDetails || null;
    },

    retrieveOrRequestAuthorizationDetails() {
        let retrievedAuthorizationDetails = this.retrieveAuthorizationDetails();
        if(!retrievedAuthorizationDetails) {
            this.requestAuthorization();
        }
        return retrievedAuthorizationDetails;
    },

    getAuthorizationHeaders() {
        let authorizationDetails = this.retrieveOrRequestAuthorizationDetails();
        if(!authorizationDetails) {
            return null;
        }
        let accessToken = authorizationDetails['access_token'];
        return {
            "Authorization": `Bearer ${accessToken}`
        };
    },

    persistAuthorizationDetails(authorizationDetails) {
        if (authorizationDetails) {
            let persistedString = sessionStorage.getItem(this.shellAuthorizationsKey) || '{}';
            let persistedObj = JSON.parse(persistedString);
            persistedObj[this.vvSiteUrl] = authorizationDetails;
            sessionStorage.setItem(this.shellAuthorizationsKey, JSON.stringify(persistedObj));
            return true;
        }
        return false;
    },

    purgeAuthorizationDetails() {
        let persistedString = sessionStorage.getItem(this.shellAuthorizationsKey) || '{}';
        let persistedObj = JSON.parse(persistedString);
        let siteAuthorizationDetails = persistedObj[this.vvSiteUrl];
        delete persistedObj[this.vvSiteUrl];
        sessionStorage.setItem(this.shellAuthorizationsKey, JSON.stringify(persistedObj));
        return siteAuthorizationDetails;
    },

    requestAuthorization(cause) {
        let message = {
            "type": this.authorizationRequestMessageType,
            "vv_site_url": this.vvSiteUrl,
            "application_id": this.applicationId,
            "status": cause || 'unavailable'  // unavailable, expired, syncing
        }
        window.top.postMessage(message, '*');
    },

    subscribeToAuthorizationChangeMessage() {
        window.addEventListener('message', ({ data }) => {
            console.log('authorization change message recieved from parent', { data });
            if(data.type == this.authorizationChangeMessageType) {
                if(data.authorized && data['authorization_details']) {
                    this.persistAuthorizationDetails(data['authorization_details']);
                    // TODO allow attaching custom callbacks
                }
                else {
                    this.purgeAuthorizationDetails();
                    // TODO
                }
            }
            
        }, false);
    }
}


/*
function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}

let currentUrl = window.location.href;
                let updatedUrl = updateQueryStringParameter(currentUrl, 'access_token', data['access_token']);
                window.location.replace(updatedUrl);

*/
