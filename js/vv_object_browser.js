
$(function() {
    // following are not declared with 'let', to access them in console during dev period.

    let shellInterface = ShellInterface.fromQueryParams();
    shellInterface.subscribeToAuthorizationChangeMessage();
    shellInterface.requestAuthorization('syncing');

    function getAuthorizationHeaders() {
        let headers = shellInterface.getAuthorizationHeaders();
        return headers;
    }

    objstore_endpoint = `${shellInterface.vvSiteUrl}/objstore/v1`;


    formRowDiv = document.getElementById("form-row");
    fdTextArea = document.getElementById("fd-text")
    objJson = null;

    Form = JSONSchemaForm.default;

    ReferrerItem = (props) => {
        console.log({ propsin: props })
        return React.createElement('li', { className: 'referrer-item-li', key: props.i },
            React.createElement('a', { href: `?resource_id=${props.referrer['_id']}`, className: 'referrer-link' }, props.referrer['_id']),
            //React.createElement('br'),
            //React.createElement('span', { className: 'referrer-json-class' }, this.props.referrer['jsonClass'])
        );
    }

    ReferrerList = (props) => {
        console.log('inside', { props: props });
        let children = props.referrers.map((referrer, i) => {
            return React.createElement(ReferrerItem, { referrer, i });
        });
        console.log({ children });
        return React.createElement('ul', { className: 'referrers-list' },
            children
        )
    }

    function onChange({formData}) {
        console.log({formData});
        $("#fd-text").text(JSON.stringify(formData, null, 2))
        //$("#fd-text").append($("<pre></pre>").text(JSON.stringify(formData, null, 2)));
    }

    function onSubmit({formData}) {
        console.log(formData);
        let postUrl = `${objstore_endpoint}/resources`;
        let postData = {
            "resources_list": JSON.stringify([formData]),
            "return_projection": JSON.stringify({"permissions": 0})
        };

        let authorizationHeaders = getAuthorizationHeaders();
        if(!authorizationHeaders) {
            $("#status-cell").text("please perform authorization....");
            return;
        }

         $("#status-cell").text("posting object....");
         $.ajax({
             url: postUrl,
             type: 'POST',
            dataType: "json",
            data: postData,
            headers: authorizationHeaders,
            success: (respData) => {
                console.log({respData});
                formElement.setState({formData: respData[0]});
                $("#status-cell").text("updated form")
            },
            error: (xhr, error) => {
                if(xhr.status == 403) {
                    $("#status-cell").text("seems authorization expired");
                    shellInterface.requestAuthorization('expired');
                }
            }
         })
    }

    function updateReferrers(lrs) {
        console.log(lrs);
        let sprs = lrs.sections.map(uid => { return { "_id": uid }; });
        let annos = lrs.annotations.map(uid => { return { "_id": uid }; });
        console.log({sprs, annos});

        sprReferrersListElement = ReactDOM.render(React.createElement(ReferrerList, { referrers: sprs }), document.getElementById('spr-col'));
        annoReferrersListElement = ReactDOM.render(React.createElement(ReferrerList, { referrers: annos }), document.getElementById('anno-col'));
    }

    function getDataAndSchema(_id) {
        let resourceGetUri = `${objstore_endpoint}/resources/${_id}`;
        let getParams = {
            projection: JSON.stringify({"permissions": 0}),
            linked_resources: JSON.stringify({"annotations": {}, "sections": {}})
        };
        $.ajax({
            url: resourceGetUri,
            type: 'GET',
            dataType: 'json',
            //headers: getHeaders,
            data: getParams,
            success: (data) => {
                updateReferrers(data['linked_resources']);
                delete data['linked_resources'];
                objJson = data;
                onChange({formData: objJson});
                objJsonClass = objJson.jsonClass;
                schemasUrl = objstore_endpoint + `/schemas/${objJsonClass}`;
                $.ajax({
                    url: schemasUrl,
                    type: "GET",
                    dataType: "json",
                    success: (schemaData) => {
                        objSchema = schemaData;
                        createForm(objSchema, objJson);
                        $("#status-cell").text("form generation successful");
                    },
                    error: () => {
                        $("#status-cell").text("error in getting schema")
                    }
                })
            },
            error: () => {
                console.log("error in getting object data")
                $("#status-cell").text("error in getting object")
            }
        })
    }

    $("#re-auth-button").click(() => {
        let initializerUrl = `initializer.html?redirect_uri=${encodeURIComponent(window.location.href)}`;
        window.location.replace(initializerUrl);
    })

    $("#button-init").click(() => {
        let _id = $("#input-id").val();
        console.log({_id});
        getDataAndSchema(_id);
    })

    function marshalSchema(schema) {
        if (Array.isArray(schema)) {
            let na = []
            for (let v of schema) {
                let nv = marshalSchema(v);
                na.push(nv);
            }
            return na;
        }
        else if (Object.prototype.isPrototypeOf(schema)) {
            //console.log("schema is object")
            let displaySchema = JSON.parse(JSON.stringify(schema));

            let allowedKeys = displaySchema['_display']
            //console.log({allowedKeys})
            if(displaySchema.properties) {
                Object.keys(displaySchema.properties).forEach((k, n) => {
                    // console.log({k, n})
                    if ( allowedKeys && allowedKeys.indexOf(k) < 0) {
                        //console.log(`deleting ${k}`)
                        delete displaySchema.properties[k]
                    }
                    else {
                        //console.log(`marshalling for ${k}`);
                        displaySchema.properties[k] = marshalSchema(displaySchema.properties[k]);
                    }
                });
            }
            else {
                Object.keys(displaySchema).forEach((k, n) => {
                    displaySchema[k] = marshalSchema(displaySchema[k]);
                });
            }
            
            return displaySchema;
        }
        else {
            return JSON.parse(JSON.stringify(schema));
        }
    }

    function createForm(schema, formData) {
        console.log({schema, formData});
        displaySchema = marshalSchema(schema);
        console.log({displaySchema});
        customForm = React.createElement(Form, {schema:displaySchema, onSubmit, onChange, formData});
        formElement = ReactDOM.render(customForm, document.getElementById('schema-form'));
    }

    queryParams = new URLSearchParams(document.location.search.substring(1));
    let resourceId = queryParams.get('resource_id');
    if(resourceId) {
        $("#input-id").val(resourceId);
        $("#button-init").click();
        // getDataAndSchema(resourceId);
    }

})
